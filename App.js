/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, View, Button, TextInput, Text} from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
  constructor(props){
    super(props)
    this.state={
      value:0
    }
  }
  add = () => this.setState(state =>({
    value:state.value +1
  }))
  substract = () => this.setState(state=>({
    value:state.value-1
  }))
  render() {
    const keyboardType = Platform.select({
      ios: 'number-and-punctuation',
      android:'number-pad'
    })
    return (
      <View style={styles.appContainer}>
      <View style={styles.header}>
        <Text>Header</Text>
      </View>
        <Text style={styles.headTitle}>Number Counter</Text>
        <View style={styles.rowContent}>
          <Button title="+" onPress={this.add.bind(this)}/>
          <TextInput 
            keyboardType={keyboardType}
            value={`${this.state.value}`}
          />
          <Button title="-" onPress={this.substract.bind(this)}/>
        </View>
        <View style={styles.footer}>
          <Text>Footer</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  appContainer: {
    flex: 1,
    // flexDirection:'column',
    display:'flex',
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    height:1280,
    backgroundColor: '#F5FCFF',
  },
  header: {
    justifyContent: 'flex-start',
    backgroundColor:'grey',
    padding:10
  },
  rowContent:{
    flexDirection: 'row',
    justifyContent:'center',
    alignItems:'center',
    flex:2
  },
  headTitle:{
    fontSize:18,
    fontWeight:'bold',
    margin:10,
    textAlign:'center'
  },
  footer: {
    justifyContent: 'flex-start',
    backgroundColor:'green',
    padding:10
  },
});
